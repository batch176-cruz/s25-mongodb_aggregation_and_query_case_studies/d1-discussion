db.fruits.insertMany([

    {

        name: "Apple",

        supplier: "Red Farms Inc.",

        stocks: 20,

        price: 40,

        onSale: true

    },

    {

        name: "Banana",

        supplier: "Yellow Farms",

        stocks: 15,

        price: 20,

        onSale: true

    },

    {

        name: "Kiwi",

        supplier: "Green Farming and Canning",

        stocks: 25,

        price: 50,

        onSale: true

    },

    {

        name: "Mango",

        supplier: "Yellow Farms",

        stocks: 10,

        price: 60,

        onSale: true

    },

    {

        name: "Dragon Fruit",

        supplier: "Red Farms Inc.",

        stocks: 10,

        price: 60,

        onSale: true

    }

])

db.fruits.find({})


// Aggregation Pipelines
    // Aggregation is typically done in 2-3 steps. Each step is called a pipeline.


db.fruits.aggregate([
    // The first pipeline is $match. $match is used to pass documents or get documents which satisfies or matches our condition.
    // Syntax: {$match: {field:<value>}}
    // $group - allows us to group together documents and create an analysis out of these grouped documents.
    // _id: in $group, we are essentially associating it to our results.
    // _id: $supplier - we grouped our matched documents based on the value of their supplier field.
    // we created a new field for this aggragated result called totalStocks.
    // We used $sum to get the sum of all the values of the grouped documents per the indicated field: $stocks.
    {$match: {onSale:true}}, // apple, mango, kiwi, dragon fruit
    // supplier: Red Farms - apple, dragon fruit - totalStock: stocks:20 + stocks:10
    // supplier: Green Farming - kiwi - totalStock: stocks:25
    // supplier: Yellow Farms - mango - totalStock: stocks:10
    {$group: {_id: "$supplier", totalStocks:{$sum: "$stocks"}}}
])

db.fruits.aggregate([    
    {$match: {onSale:true}}, 
    // Match Stage:
    // apple, mango, kiwi, dragon fruit
    
    // When you add a definite value for the _id in group stage, we will have only 1 group from our matched documents. 
    // Same goes if the _id is null.
    // What if the _id provided, is a value common to some of our documents. It will be considered as a definite value and just grouped our documents in a single group.
    
    // totalOnSaleStocks, totalStocks:{stocks: stocks:20 + stocks:25 + stocks:10 + stocks:10 + stocks:15}
    {$group: {_id: "totalOnSaleStocks", totalStocks:{$sum: "$stocks"}}}
])
    
db.fruits.aggregate([    
    {$match: {onSale:true}}, 
    {$group: {_id: null, totalStocks:{$sum: "$stocks"}}}
])
    
db.fruits.aggregate([    
    {$match: {onSale:true}}, 
    {$group: {_id: "Yellow Farms", totalStocks:{$sum: "$stocks"}}}
])

db.fruits.aggregate([    
    {$match: {supplier:"Yellow Farms"}}, // banana, mango
    {$group: {_id: "YellowFarmsStock", totalStocks:{$sum: "$stocks"}}}
])
    



// Mini-Activity:
// In a new aggregation for the fruits collection:
// Get the total stocks of items supplied by Red Farms.

db.fruits.aggregate([    
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id: "RedFarmsStock", totalStocks:{$sum: "$stocks"}}}
])
    
db.fruits.aggregate([    
    {$match: {supplier:"Red Farms Inc."}},
    {$match: {onSale:true}},
    {$group: {_id: "RedFarmsStock", totalStocks:{$sum: "$stocks"}}}
])
    
db.fruits.aggregate([    
    {$match: {supplier:"Red Farms Inc.", onSale: true}},
    {$group: {_id: "RedFarmsStock", totalStocks:{$sum: "$stocks"}}}
])

db.fruits.aggregate([    
    {$match: {supplier:{$regex:'yellow',$options:'$i'}}},
    {$group: {_id: "yellowSupplier", totalStocks:{$sum: "$stocks"}}}
])  
    




// $avg - an operator mostly used for the $group stage.
// $avg - gets the avg of the numerical values of the indicated field in grouped documents.
    
db.fruits.aggregate([
    {$match:{onSale:true}},
    // suppliers: Red Farms Inc. - apple - avgStock: stocks:20 + stocks:10/<numberOfDocuments>
    // suppliers: Yellow Farms - banana, mango - avgStock: stocks:15 + stocks:10/<numberOfDocuments>
    {$group:{_id:"$supplier", avgStock:{$avg: "$stocks"}}}   
])
    
    
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier", avgPrice:{$avg: "$price"}}}   
])







// $max - will allows us to get the highest value out of all the values in a given field in grouped documents

// highest number of stock for all items on sale:
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"highestStockOnSale", maxStock:{$max:"$stocks"}}}
])
    
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier", maxStock:{$max:"$stocks"}}}
])
    
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$name", maxStock:{$max:"$stocks"}}}
])




// $min - will allow us to get the lowest value of the values in the given field in grouped documents
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"minsStockOnSale", minStock:{$min:"$stocks"}}}   
])



// Mini-Activity
// Get the lowest number of stocks for all items which price is lower than 50

db.fruits.aggregate([
    {$match:{price:{$lt:50}}},
    {$group:{_id:"minsStockWithPriceLowerThan50", minStock:{$min:"$stocks"}}}   
])




// Other Stages
// $count - is a stage we can add after a match stage to count all the items that matches our criteria
db.fruits.aggregate([
    {$match: {onSale:true}},
    // match - apple,kiwi,mango,banana,dragon fruit
    {$count: "itemsOnSale"}
    // numberOfDocuments that matched - 5
])
    
    


db.fruits.aggregate([
    {$match:{price:{$lt:50}}},
    {$count: "itemPriceless50"}
])


// Number of items with stocks less than 20
db.fruits.aggregate([
    {$match: {stocks:{$lt:20}}},
    {$count: "forRestock"}
])




// $out - will save/output the result in a new collection.
// note: This will overwrite the collection if it already exists.
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStock:{$sum: "$stocks"}}},
    // $out: <collectionNameOfResults>
    {$out: "stockSupplier"}
      
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStock:{$max: "$stocks"}}},
    // $out: <collectionNameOfResults>
    {$out: "stockSupplier"}     
]) 
    





db.fruits.aggregate([
    
    {$match: {onSale:true}},
    {$group: {_id:"$supplier"}},
    {$count: "numberOfSuppliers"}

])